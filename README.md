# GRIN-Global #

GRIN-Global (GG) is a plant genebank management system. The initial GG project involved the **USDA
Agricultural Research Service**, **Bioversity International**, and the **Global Crop Diversity Trust**.

***GRIN-Global is public-domain software freely available to the world’s crop genebanks .
***
##GRIN-Global has two components:##
* End User Components
* Server Components
 
The repository contain the source code of GRIN-Global Admin and GRIN-Global Public website,***version 1.9.1*** . These components are part of GRIN-Global **Server Components**.

### How do I get set up? ###

* [Install GRIN-Global server components](http://www.ars-grin.gov/npgs/gringlobal/docs/gg_installation_guide.pdf).
* In order to work with **Server Components** source code, you need to have installed ***Microsoft Visual Studio 2008***.


### Contribution ###

* For any contribution to this source code, please, contact CIMMYT-GRINGlobal-International@cgiar.org